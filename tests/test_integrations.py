import os
import pytest
import requests
from app import app
from flask import url_for


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client


def test_acceuil(client):
    response = client.get('/')
    responseJSON = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    assert 200 == response.status_code
    assert 200 == responseJSON.status_code